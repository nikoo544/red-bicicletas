class Bicicleta {
    constructor(id, color, modelo, ubicacion) {
        this.id = id;
        this.color = color;
        this.modelo = modelo;
        this.ubicacion = ubicacion;
    }

}

Bicicleta.prototype.toString = () => {
    return 'id: ' + this.id + " | color: " + this.color;
};

Bicicleta.allBicis = [];

Bicicleta.add = (bike) => {
    Bicicleta.allBicis.push(bike);
};

Bicicleta.findById = (biciId) => {

    let bici = Bicicleta.allBicis.find(x => x.id == biciId);
    if (bici)
        return bici;
    else
        throw new Error(`No existe una bicicleta con el id ${biciId}`);
};

Bicicleta.removeById = (biciId) => {
    for (let i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == biciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
};


var a = new Bicicleta(1, 'rojo', 'urbana', [-33.739602, -61.960342]);
var b = new Bicicleta(2, 'blanca', 'mountain', [-33.744284, -61.968397]);
var c = new Bicicleta(3, 'verde', 'urbana', [-33.751378, -61.970163]);
var d = new Bicicleta(4, 'amarilla', 'playera', [-33.739120, -61.966499]);
//
// Bicicleta.add(a);
// Bicicleta.add(b);
// Bicicleta.add(c);
// Bicicleta.add(d);

module.exports = Bicicleta;



