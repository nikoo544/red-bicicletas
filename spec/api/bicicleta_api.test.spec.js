let Bicicleta = require('../../models/bicicleta');
let request = require('request');
let server = require('../../bin/www');

describe('Bicicleta API', () => {

    describe('GET BICICLETAS /', () => {
        it('should be Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            let a = new Bicicleta(1, 'negro', 'uruguaya', [-14, -45]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', (error, response, body) => {
                expect(response.statusCode).toBe(200);
            });

        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Should be Status 200', (done) => {
            let headers = {'content-type': 'application/json'};
            let bici = '{"id":"10","color":"rojo","modelo":"urbana","latitud":"34","longitud":"-54"}';

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: bici
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });
    });
});