let Bicicleta = require('../../models/bicicleta');

beforeEach(()=> {Bicicleta.allBicis = [];})

describe('Bicicletas.allBicis', () => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicletas.add', () => {
    it('Agregamos una', () => {

        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana', [-33.739602, -61.960342]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findByID', () => {
    it('Debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        let Bici1 = new Bicicleta(1, 'verde', 'urbana');
        let Bici2 = new Bicicleta(1, 'amarillo', 'urbana');
        Bicicleta.add(Bici1);
        Bicicleta.add(Bici2);

        let targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(Bici1.color);
        expect(targetBici.modelo).toBe(Bici1.modelo);
    });
});