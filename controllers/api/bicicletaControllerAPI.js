const Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = (req, res) => {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
};

exports.bicicleta_create = (req, res) => {
    let bike = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bike.ubicacion = [req.body.latitud, req.body.longitud];

    Bicicleta.add(bike);

    res.status(200).json({
        bicicleta: bike
    });
};

exports.bicicleta_delete = (req, res) => {
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
};

exports.bicicleta_update = (req, res) => {
    let bike = Bicicleta.findById(req.body.id);
    bike.id = req.body.id;
    bike.color = req.body.color;
    bike.modelo = req.body.modelo;
    bike.ubicacion = [req.body.latitud, req.body.longitud];

    res.status(200).json({
        bicicleta: bike
    });

};