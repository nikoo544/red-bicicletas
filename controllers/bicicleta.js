const Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = (req, res) => {
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
};

exports.bicicleta_create_get = (req, res) => {
    res.render('bicicletas/create');
};

exports.bicicleta_create_post = (req, res) => {
    let bike = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bike.ubicacion = [req.body.latitud , req.body.longitud];
    Bicicleta.add(bike);

    res.redirect('/bicicletas');
};


exports.bicicleta_update_get = (req, res) => {
    let bike = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {bike});
};

exports.bicicleta_update_post = (req, res) => {
    let bike =  Bicicleta.findById(req.params.id);
    bike.id = req.body.id;
    bike.color = req.body.color;
    bike.modelo = req.body.modelo;
    bike.ubicacion = [req.body.latitud , req.body.longitud];

    res.redirect('/bicicletas');
};

exports.bicicleta_delete_post = (req,res)=>{
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}
